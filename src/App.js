import React, { Component } from 'react';
import {
  BrowserRouter as Router, 
  Route,
  IndexRoute,
} from 'react-router-dom'

import logo from './logo.svg';
import './App.css';
import NavBar from './stories/NavBar.jsx';
import Infos from './stories/Infos.jsx';
import HomePage from './stories/HomePage.jsx';

import Cholesky from './stories/Cholesky.jsx';
import Determinant from './stories/Determinant.jsx';
import Inverse from './stories/Inverse.jsx';
import Gauss from './stories/Gauss.jsx';
import Lu from './stories/Lu.jsx';
import LuFacto from './stories/LuFacto.jsx';
import CholeskyFacto from './stories/CholeskyFacto.jsx';

class App extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      thisMatrix: [],
      thisVector: []
    }
  }
  render() {
    return (
      <Router>
        <div>
          <Route path="/welcome" component={HomePage} />
          <Route path="/welcome/cholesky" component={Cholesky} />
          <Route path="/welcome/inverse" component={Inverse} />
          <Route path="/welcome/determinant" component={Determinant} />
          <Route path="/welcome/gauss" component={Gauss} />
          <Route path="/welcome/lu" component={Lu} />
          <Route path="/welcome/lufacto" component={LuFacto} />
          <Route path="/welcome/choleskyfacto" component={CholeskyFacto} />
          <Route path="/infos" component={Infos} />
        </div>
      </Router>
    );
  }
}

export default App;
