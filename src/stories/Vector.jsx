import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    Input, Button
} from 'semantic-ui-react'

const styles = {
    inputVector: {
        width: "60px",
    }
}

export default class Vector extends Component {
    
    constructor(props) {
        super(props)
        let vectorInit = []
        for(let i=0; i<this.props.size; i++) {
            let vectorInit = 10000 
        }
        this.state = {
            result: 0,
            vector: vectorInit,
            
        }
    }

    handleVectorChange(e) {
        const { onChange } = this.props
        let tmpVector = this.state.vector
        const name = e.target.name
        tmpVector[e.target.name] = parseFloat(e.target.value)
        this.setState({
            vector: tmpVector
        })
        onChange(tmpVector)
    }

    handleChange(e) {
        this.setState({
            size: e.target.value
        }, () => {
            let newMatrix = new Array(this.state.size)
            this.setState({ matrix: newMatrix})
        })
    }

    render() {

        let inputVector = []
        for (let i = 0; i < this.state.size; i++) {
                inputVector.push(
                    <Input
                        required
                        type="number"
                        onChange={this.handleVectorChange.bind(this)}
                        size="mini"
                        key={i}
                        name={i}
                        style={styles.inputVector}
                        placeholder={"["+i+"]"}
                    />
                )
            inputVector.push(<br key={"br-"+i}/>)
        }

        return(
            <div className="vector-container">
                <Input placeholder="Taille du vecteur" onChange={this.handleChange.bind(this)} />
                <br/>
                <br/>
                {inputVector}
                <br/>
            </div>
        )
    }
}

Vector.propTypes = {
    size: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
}