import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Request from 'superagent'
import {
    Button, Input, TextArea, Form, Grid
} from 'semantic-ui-react'

import Matrix from './Matrix.jsx';


const styles = {
  centerAll: {
    marginTop: "150px"
  }
}

export default class Lu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      thisMatrix: [],
      L: [],
      U: [],
      display: "Résultat",
      size: 0,
    }
  }
      
  handleCalc() {
    var that = this
    Request
        .post('http://127.0.0.1:5000/api/lufacto')
        .send(that.state.thisMatrix)
        .end(function(err, res){
            const response = JSON.parse(res.text)
            that.setState({L: response[0]})
            that.setState({U: response[1]})
            let displayTmp = "L:\n[\n"
            for(let i=0; i<that.state.L.length; i++) {
                displayTmp += "\t["
                for(let j=0; j<that.state.L.length; j++) {
                    displayTmp += that.state.L[i][j].toFixed(2)+",   "
                }
                displayTmp += "]\n"
            }
            displayTmp += "]\n"
            displayTmp += "U:\n[\n"
            for(let i=0; i<that.state.U.length; i++) {
                displayTmp += "\t["
                for(let j=0; j<that.state.U.length; j++) {
                    displayTmp += that.state.U[i][j].toFixed(2)+",   "
                }
                displayTmp += "]\n"
            }
            displayTmp += "]"
            that.setState({
              display: displayTmp
            })
        })
  }

  render() {
    return (
      <Grid centered columns={3} style={styles.centerAll}>
        <Grid.Column>
          <Matrix size={this.state.size} onChange={matrix => this.setState({thisMatrix: matrix})} />
          <Button primary onClick={this.handleCalc.bind(this)}>Calculer</Button>
          <br/>
          <br/>
          <Form>
            <TextArea autoHeight value={this.state.display} 
            />
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}
