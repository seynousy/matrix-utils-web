import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';
import 'semantic-ui-css/semantic.min.css';

import Infos from './Infos.jsx';
import Matrix from './Matrix.jsx';
import NavBar from './NavBar.jsx'
import Vector from './Vector.jsx'
import SizeInput from './SizeInput.jsx'
import Determinant from './Determinant.jsx'
import Inverse from './Inverse.jsx'
import Cholesky from './Cholesky.jsx'


storiesOf('Infos', module)
  .add('basic', () => (
    <Infos />
  ));

storiesOf('Matrix', module)
  .add('with 5x5 entries', () => (
    <Matrix size={5} />
  ))
  .add('with 10x10 entries', () => (
    <Matrix size={10} />
  ))
  .add('as a Vector 5', () => (
    <Vector size={5} />
  ));

storiesOf('NavBar', module)
  .add('basic', () => (
    <NavBar />
  ));
  
storiesOf('SizeInput', module)
  .add('basic', () => (
    <SizeInput sizeValue={5}/>
  ));

storiesOf('Determinant', module)
  .add('basic', () => (
    <Determinant />
  ));

storiesOf('Inverse', module)
  .add('basic', () => (
    <Inverse />
  ));

storiesOf('Cholesky', module)
  .add('basic', () => (
    <Cholesky />
  ));
