import React, { Component } from 'react'
import {
    Button, Form, Card, Grid
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const startQuote = "Commencer"
const nameQuote = "Nom"
const phoneQuote = "Numéro de Téléphone"

const styles = {
    startButton: {
        textAlign: "center"
    },
    header: {
        textAlign: "center"
    },
    absoluteCenter: {
        marginTop: "150px"
    }
}

export default class Infos extends Component {
    render() {
        return(
            <div style={styles.absoluteCenter}>
            <Grid columns={5} centered>
                <Grid.Column>
                    <Form>
                        <Card>
                            <Card.Content style={styles.header}>
                                <Card.Header>Informations</Card.Header>
                            </Card.Content>
                            <Card.Content>
                                <Form.Field>
                                    <input name="name" placeholder={nameQuote} />
                                </Form.Field>
                                <Form.Field>
                                    <input name="mobile" placeholder={phoneQuote} />
                                </Form.Field>
                            </Card.Content>
                            <Card.Content style={styles.startButton}>
                                <Link to="/welcome">
                                    <Button primary type="submit">
                                        {startQuote}
                                    </Button>
                                </Link>
                            </Card.Content>
                        </Card>
                    </Form>
                </Grid.Column>
            </Grid>
            </div>
        )
    }
}