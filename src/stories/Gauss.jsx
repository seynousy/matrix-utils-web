import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Request from 'superagent'
import {
    Button, Input, TextArea, Form, Grid
} from 'semantic-ui-react'

import Matrix from './Matrix.jsx';
import Vector from './Vector.jsx';


const styles = {
  centerAll: {
    marginTop: "150px"
  }
}

export default class Gauss extends Component {
  constructor(props) {
    super(props)
    this.state = {
      thisMatrix: [],
      thisVector: [],
      matrixResult: [],
      display: "Résultat",
      size: 0,
    }
  }

  handleCalc() {
    const toPost = [this.state.thisMatrix, this.state.thisVector]
    var that = this
    Request
        .post('http://127.0.0.1:5000/api/gauss')
        .send(toPost)
        .end(function(err, res){
            that.setState({matrixResult: JSON.parse(res.text)})
            let displayTmp = "x:\n["
            for(let i=0; i<that.state.matrixResult.length; i++) {
                displayTmp += that.state.matrixResult[i].toFixed(2) + ",   "
            }
            displayTmp += "]"
            that.setState({
              display: displayTmp
            })
        })
  }

  render() {
    return (
      <Grid centered columns={3} style={styles.centerAll}>
        <Grid.Column>
          <Matrix size={this.state.size} onChange={matrix => this.setState({thisMatrix: matrix})} />
          <Vector size={this.state.size} onChange={vector => this.setState({thisVector: vector})} />
          <Button primary onClick={this.handleCalc.bind(this)}>Calculer</Button>
          <br/>
          <br/>
          <Form>
            <TextArea autoHeight value={this.state.display} 
            />
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}
