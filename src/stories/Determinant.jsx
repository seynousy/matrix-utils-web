import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Request from 'superagent'
import {
    Button, Input, Grid
} from 'semantic-ui-react'

import Matrix from './Matrix.jsx';

const styles = {
  centerAll: {
    marginTop: "150px"
  }
}

export default class Determinant extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      thisMatrix: [],
      result: 0
    }
  }

  handleCalc() {
    var that = this
    Request
        .post('http://127.0.0.1:5000/api/determinant')
        .send(that.state.thisMatrix)
        .end(function(err, res){
            that.setState({result: res.text}) 
        });
  }

  render() {
    return (
      <Grid columns={3} centered style={styles.centerAll}>
        <Grid.Column>
          <Matrix size={3} onChange={matrix => this.setState({thisMatrix: matrix})} />
          <Button primary onClick={this.handleCalc.bind(this)}>Calculer</Button>
          <br/>
          <br/>
          <Input value={this.state.result} />
        </Grid.Column>
      </Grid>
    );
  }
}
