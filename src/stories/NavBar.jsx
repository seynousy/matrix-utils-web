import React, { Component } from 'react'
import {
    Button, Dropdown, Menu
} from 'semantic-ui-react'

import { Link } from 'react-router-dom'

export default class NavBar extends Component {
    state = { activeItem: "home"}

    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    handleHelpClick() {

    }

    render() {
        const { activeItem } = this.state
        return(
            <Menu>
                <Menu.Item name="home" onClick={this.handleItemClick}>MatrixUtils</Menu.Item>
                <Menu.Item name="home" onClick={this.handleItemClick}>
                    <Link to="/welcome/determinant">
                        Determinant
                    </Link>
                </Menu.Item>
                <Menu.Item name="home" onClick={this.handleItemClick}>
                    <Link to="/welcome/inverse">
                        Inverse
                    </Link>
                </Menu.Item>
                <Menu.Menu>
                    <Dropdown item text="Résolution Système Linéaire">
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                <Link to="/welcome/lu">
                                    LU
                                </Link>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <Link to="/welcome/cholesky">
                                    Cholesky
                                </Link>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <Link to="/welcome/gauss">
                                    Gauss
                                </Link>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
                <Menu.Menu>
                    <Dropdown item text="Factorisation">
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                <Link to="/welcome/lufacto">
                                    LU
                                </Link>
                            </Dropdown.Item>
                            <Dropdown.Item>
                                <Link to="/welcome/choleskyfacto">
                                    Cholesky
                                </Link>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Menu.Menu>
                <Menu.Menu position="right">
                    <Dropdown item text="Langues">
                        <Dropdown.Menu>
                            <Dropdown.Item>Français</Dropdown.Item>
                            <Dropdown.Item>Wolof</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                    <Menu.Item onClick={this.handleHelpClick}>Aide</Menu.Item>
                </Menu.Menu>
            </Menu>
        )
    }
}