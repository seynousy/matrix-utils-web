import React, { Component } from 'react'
import NavBar from './NavBar.jsx'
import Infos from './Infos.jsx'
import {
    Grid, Image
} from 'semantic-ui-react'

export default class HomePage extends Component {
    render() {
        return(
            <Grid centered columns={1}>
                <Grid.Column>
                    <NavBar />
                </Grid.Column>
            </Grid>
        )
    }
}