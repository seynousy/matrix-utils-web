import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Request from 'superagent'
import {
    Button, Input, TextArea, Form, Grid
} from 'semantic-ui-react'

import Matrix from './Matrix.jsx';

const styles = {
  centerAll: {
    marginTop: "150px"
  }
}

export default class Inverse extends Component {
  constructor(props) {
    super(props)
    this.state = {
      thisMatrix: [],
      matrixResult: [],
      display: "Résultat"
    }
  }

  handleCalc() {
    var that = this
    Request
        .post('http://127.0.0.1:5000/api/inverse')
        .send(that.state.thisMatrix)
        .end(function(err, res){
            that.setState({matrixResult: JSON.parse(res.text)})
            let displayTmp = "Inverse:\n[\n"
            for(let i=0; i<that.state.thisMatrix.length; i++) {
              displayTmp += "\t["
              for(let j=0; j<that.state.thisMatrix.length; j++) {
                displayTmp += that.state.matrixResult[i][j].toFixed(2)+",   "
              }
              displayTmp += "]\n"
            }
            displayTmp += "\n]"
            that.setState({
              display: displayTmp
            })
        })
  }

  render() {
    return (
      <Grid centered columns={3} style={styles.centerAll}>
        <Grid.Column>
          <Matrix size={this.state.size} onChange={matrix => this.setState({thisMatrix: matrix})} />
          <Button primary onClick={this.handleCalc.bind(this)}>Calculer</Button>
          <br/>
          <br/>
          <Form>
            <TextArea autoHeight value={this.state.display} 
            />
          </Form>
        </Grid.Column>
      </Grid>
    );
  }
}
