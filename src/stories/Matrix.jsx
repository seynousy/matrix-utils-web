import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    Input, Button
} from 'semantic-ui-react'

const styles = {
    inputMatrix: {
        width: "60px",
    }
}

export default class Matrix extends Component {
    
    constructor(props) {
        super(props)
        // let matrixInit = []
        // for (let i = 0; i < 3; i++) {
        //     let matrixJ = []
        //     for (let j = 0; j < 3; j++) {
        //         matrixJ[j] = 0;
        //     }
        //     matrixInit[i] = matrixJ
        // }
        this.state = {
            result: 0,
            matrix: [],
            size: this.props.size,
            
        }
    }

    handleMatrixChange(e) {
        const { onChange } = this.props;
        let matrixInit = this.state.matrix
        const name = e.target.name
        const i = parseInt(name.substring(0, 1))
        const j = parseInt(name.substring(2, 3))
        matrixInit[i][j] = parseFloat(e.target.value)
        this.setState({
            matrix: matrixInit
        })
        onChange(matrixInit)
    }

    handleChange(e) {
        this.setState({
            size: e.target.value
        }, () => {
            let newMatrix = new Array(this.state.size)
            for (let i = 0; i < this.state.size; i++) {
                newMatrix[i] = new Array(this.state.size)
            }
            this.setState({ matrix: newMatrix})
        })
    }

    render() {

        let inputMatrix = []
        for (let i = 0; i < this.state.size; i++) {
            for (let j = 0; j < this.state.size; j++) {
                inputMatrix.push(
                    <Input
                        required
                        type="number"
                        onChange={this.handleMatrixChange.bind(this)}
                        size="mini"
                        key={i+"-"+j}
                        name={i+"-"+j}
                        style={styles.inputMatrix}
                        placeholder={"["+i+","+j+"]"}
                    />
                )
            }
            inputMatrix.push(<br key={"br-"+i}/>)
        }

        return(
            <div className="matrix-container">
                <Input placeholder="Taille de la matrice" onChange={this.handleChange.bind(this)} />
                <br/>
                <br/>
                {inputMatrix}
                <br/>
            </div>
        )
    }
}

Matrix.propTypes = {
    size: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
}